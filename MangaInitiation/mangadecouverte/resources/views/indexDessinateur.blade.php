@extends('layouts.layout')
@section('titrePage')
Liste des dessinateurs :
@endsection

@section('titreItem')
Tous les dessinateurs :
@endsection


@section('contenu')
<table class="table table-dark table-striped">
	<thead>
		<th>ID</th>
		<th>Nom</th>
		<th>Prénom</th>
	</thead>
	@foreach ($dessinateurs as $dessinateur)
		<tr>
			<td> {{ $dessinateur ->id_dessinateur }} </td>
			<td> {{ $dessinateur ->nom_dessinateur }} </td>
			<td> {{ $dessinateur ->prenom_dessinateur }} </td>
		</tr>
	@endforeach
</table>
@endsection