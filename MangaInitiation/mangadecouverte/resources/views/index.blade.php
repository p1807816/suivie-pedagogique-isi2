@extends('layouts.layout')
@section('titrePage')
Liste des mangas :
@endsection

@section('titreItem')
Tous les Mangas
@endsection


@section('contenu')
<table class="table table-dark table-striped">

	<thead>
		<th>ID</th>
		<th>Titre</th>
		<th>Prix</th>
		<th>Couverture</th>
		<th>Dessinateur Nom</th>
		<th>Dessinateur Prénom</th>
	</thead>
	
	
	@foreach ($mangas as $manga)
		<tr>
			<td> {{ $manga ->id_manga }} </td>
			<td> {{ $manga ->titre }} </td>
			<td> {{ $manga ->prix }} </td>
			<td> {{ $manga ->couverture }} </td>
			<td>{{$manga->nom_dessinateur}}</td>
			<td>{{$manga->prenom_dessinateur}}</td>
		</tr>

	@endforeach

	
</table>
@endsection


