<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestFormController extends Controller
{
    //controller pour manip formulaire
    
    /**getInfo : affiche le formulaire au client (GET) */
    public function getInfos(){
        //retourné dans la vue monFormulaire.blade.php
        return view('monFormulaire');
    }

    /**postInfo : le client soumet son formulaire (POST) */
    public function postInfos(Request $request){
        //pas de vue crée après envoi formulaire, on affiche ce qui a était envoyé :
        return 'Le message saisi dans la zone de texte est : '.$request->input('message');
    }
}