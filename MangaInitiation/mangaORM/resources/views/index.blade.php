@extends('layouts.layout')
@section('titrePage')
Liste des mangas :
@endsection

@section('titreItem')
Tous les Mangas
@endsection


@section('contenu')

@if(session()->has('info'))
<div class="card text-white bg-success mb-3" style="max-width: 18rem">
    <div class="card-body">
        <p class="card-text">{{session('info')}}</p>
    </div>
</div>
@endif

<table class="table table-dark table-striped">

    <thead>

        <th>#</th>
        <th>Titre</th>

        <th>Genre</th>
       
        <th></th>
        @auth
            <th></th>
            <th>
            
                <th><a class="btn btn-sucess" href="{{route('mangas.create')}}"></a></th>
                
            </th>
        @endauth
        <!-- <th>
            <form action="{{route('mangas.create')}}">
                @csrf
                @method('DELETE')
                <button class="btn btn-success">Nouveau Manga</button>
            </form>
        </th> -->

    </thead>


    @foreach ($mangas as $manga)
    <tr>

        <td> {{ $manga ->id }} </td>
        <td> {{ $manga ->titre }} </td>

        <td> {{ $manga ->genre }} </td>
        <td>
            <a class="btn btn-primary" href="{{route('mangas.show', $manga->id)}}">
                <button class="btn btn-primary">voir</button>
            </a>
        </td>
        @auth
        <td>
            <a class="btn btn-warning" href="{{route('mangas.edit', $manga->id)}}">
                Modifier
            </a>
        </td>
        <td>
            <form action="{{route('mangas.destroy' , $manga->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger">Supprimer</button>
            </form>
        </td>
        @endauth
    </tr>

    @endforeach


</table>
@endsection