@extends('layouts.layout')
@section('titrePage')
Liste des mangas :
@endsection

@section('titreItem')
Info sur le manga
@endsection


@section('contenu')
<div class="card">
    <header class="card-header">
        <h5 class="card-header-title">Titre : {{$manga->titre}}</h5>
    </header>
    <div class = "card-content">
        <div class="content">
            <p>Dessinateur : {{$manga->nomDessinateur}}</p>
            <p>Scenariste : {{$manga->nomScenariste}}</p>
            <p>Genre : {{$manga->genre}}</p>
            <hr>
            <p>Prix : {{$manga->prix}}</p>
        </div>
    </div>
</div>
@endsection