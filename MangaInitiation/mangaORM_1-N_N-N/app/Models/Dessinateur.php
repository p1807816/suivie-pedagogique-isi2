<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Dessinateur extends Model
{
    use HasFactory;
    //ne prend pas en charge le timestamp : 
    public $timestamp =false;
    protected $fillable = ['nom_dessinateur', 'prenom_dessinateur'];

    //assesseur : 
    public function mangas(){
        
        return $this->hasMany(Manga::class);
    }
}
