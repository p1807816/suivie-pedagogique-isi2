<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Manga extends Model
{
    use HasFactory;
    //ne prend pas en charge le timestamp : 
    public $timestamp =false;


    //assesseurs : 
    public function dessinateur(){
        
        return $this->belongsTo(Dessinateur::class);
    }

    public function scenariste(){
        
        return $this->belongsTo(Scenariste::class);
    }

    public function genre(){
        
        return $this->belongsTo(Genre::class);
    }

    public function collections(){
        return $this->belongsToMany(collections::class);
    }

}
