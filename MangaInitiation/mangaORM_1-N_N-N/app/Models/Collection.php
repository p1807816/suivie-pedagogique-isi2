<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    //ne prend pas en charge le timestamp : 
    public $timestamp =false;

    //assesseur : 
    public function mangas(){
        
        return $this->belongsToMany(Manga::class);
    }
}
