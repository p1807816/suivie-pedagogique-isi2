<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
   //ne prend pas en charge le timestamp : 
   public $timestamp =false;
   protected $fillable = ['lib_genre'];

   //assesseur : 
   public function mangas(){
       
       return $this->hasMany(Manga::class);
   }
}
