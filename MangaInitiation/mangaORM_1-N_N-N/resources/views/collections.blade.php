@extends('layouts.layout')
@section('titrePage')
Liste des mangas :
@endsection

@section('titreItem')
Tous les Mangas
@endsection


@section('contenu')

    @if(session()->has('info'))
        <div class="card text-white bg-success mb-3" style="max-width: 18rem">
            <div class="card-body">
                <p class="card-text">{{session('info')}}</p>
            </div>
        </div>
    @endif

<table class="table table-dark table-striped">

	<thead>
		
        <th>#</th>
		<th>Collection</th>
		
		<th>Prix</th>


        <th></th>
		
	</thead>
	
	
	@foreach ($collections as $collection)
		<tr>
		
            <td> {{ $collection->id }} </td>
			<td> {{ $collection->titre }} </td>
			
			<td> {{ $collection->prix}} </td>
			<td>
                <a class="btn btn-primary" href="{{route('collections.show', $collection->id)}}">
                    <button class ="btn btn-primary">voir</button>
                </a>
            </td>
        
		</tr>

	@endforeach

	
</table>
@endsection