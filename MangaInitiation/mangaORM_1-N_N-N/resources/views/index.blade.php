@extends('layouts.layout')
@section('titrePage')
Liste des mangas :
@endsection

@section('titreItem')
Tous les Mangas
@endsection


@section('contenu')

    @if(session()->has('info'))
        <div class="card text-white bg-success mb-3" style="max-width: 18rem">
            <div class="card-body">
                <p class="card-text">{{session('info')}}</p>
            </div>
        </div>
    @endif

<table class="table table-dark table-striped">

	<thead>
		
        <th>#</th>
		<th>Titre</th>
		
		<th>Genre</th>

        <th></th>

		
	</thead>
	
	
	@foreach ($mangas as $manga)
		<tr>
		
            <td> {{ $manga ->id }} </td>
			<td> {{ $manga ->titre }} </td>
			
			<td> {{ $manga ->genre->lib_genre }} </td>
			<td>
                <a class="btn btn-primary" href="{{route('mangas.show', $manga->id)}}">
                    <button class ="btn btn-primary">voir</button>
                </a>
            </td>
        
		</tr>

	@endforeach

	
</table>
@endsection